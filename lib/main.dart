import 'package:flutter/material.dart';
import 'package:simple_traps/countDown.dart';
import 'package:simple_traps/randomFloor.dart';
import 'package:simple_traps/twoPartyFloor.dart';
import 'package:package_info_plus/package_info_plus.dart';

void main() {
  runApp(MaterialApp(
    title: 'Simple traps',
    home: Home()
  ));


}

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Flutter FlatButton Example'),
          ),
          body: Center(child: Column(children: <Widget>[
            Container(
              margin: EdgeInsets.all(5),
              child: FlatButton(
                child: Text('Count down', style: TextStyle(fontSize: 20.0),),
                color: Colors.blueAccent,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CountDown()),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: FlatButton(
                child: Text('Random floor', style: TextStyle(fontSize: 20.0),),
                color: Colors.blueAccent,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RandomFloor()),
                  );
                },
              ),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: FlatButton(
                child: Text('2 party floor', style: TextStyle(fontSize: 20.0),),
                color: Colors.blueAccent,
                textColor: Colors.white,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TwoPartyFloor()),
                  );
                },
              ),
            ),
          ]
          )
          )
      ),
    );
  }
}