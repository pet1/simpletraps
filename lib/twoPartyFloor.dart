import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TwoPartyFloor extends StatefulWidget {
  @override
  _TwoPartyFloor createState() => new _TwoPartyFloor();
}

class _TwoPartyFloor extends State<TwoPartyFloor> {
  late TextEditingController _widthController;
  late TextEditingController _heightController;
  late TextEditingController _firstController;
  late TextEditingController _secondsController;


  Color defaultColor = Colors.white;
  Color FirstColor = Colors.red;
  Color SecondColor = Colors.blue;

  @override
  void initState() {
    super.initState();
    _widthController = new TextEditingController(text: '5');
    _heightController = new TextEditingController(text: '5');
    _firstController = new TextEditingController(text: '5');
    _secondsController = new TextEditingController(text: '5');
  }

  List<Widget> list = List.empty();


  List<Widget> generateList() {
    int FirstAmount = int.parse(_firstController.value.text);
    int SecondAmount = int.parse(_secondsController.value.text);
    int Width = int.parse(_heightController.value.text);
    int Height = int.parse(_heightController.value.text);


    int size = Height*Width;
    List<Color> colors = List<Color>.filled(size, defaultColor, growable: false);
    colors.fillRange(0, FirstAmount, FirstColor);
    colors.fillRange(FirstAmount, FirstAmount+SecondAmount, SecondColor);
    colors.shuffle();

    var tempList = List.generate(size, (index) {
      return Container(
        child: Card(
          color: colors[index],
        ),
      );
    });
    return tempList;
  }

  Widget build(BuildContext context) {
    int Width = int.parse(_heightController.value.text);

    return new Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
          title: Text("Timer test"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) => _buildPopupDialog(context),
            );
          },
        ),
    ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              height: 500,
              width: 500,
              child: GridView.count(
                  primary: true,
                  crossAxisCount: Width,
                  children:  list
              ),
            ),
            Container(
              child: RaisedButton(
                child: Text("Generate"),
                onPressed: (){
                  setState(() {
                    list = generateList();
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Settings'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createSettingField(_widthController, "Width"),
          createSettingField(_heightController, "Height"),
          createSettingField(_firstController, "Players on team 1"),
          createSettingField(_secondsController, "Players on team 2"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Ok'),
        ),
      ],
    );
  }

  Padding createSettingField(TextEditingController controller, String label){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        onChanged: (String value) async {
          setState(() {
            controller.text = value;
          });
        },
        controller: controller,
        decoration: InputDecoration(
          labelText: label,
          border: OutlineInputBorder(
          ),
        ),
        keyboardType: TextInputType.number,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
        ], // Only numbers can be entered
      ),
    );
  }
}