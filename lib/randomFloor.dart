import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutter/services.dart';

class RandomFloor extends StatefulWidget {
  @override
  _RandomFloor createState() => new _RandomFloor();
}

class _RandomFloor extends State<RandomFloor> {
  late TextEditingController _percentController;
  late TextEditingController _widthController;
  late TextEditingController _heightController;
  Color defaultColor = Colors.white;
  Color dangerColor = Colors.red;
  int width = 5;
  int height = 5;
  int chance = 33;


  GridView grid = new GridView.count(crossAxisCount: 5);

  GridView generateGrid() {
    var rng = new Random();
    double screenWidth = MediaQuery. of(context). size. width;
    double screenHeight = MediaQuery. of(context). size. height;
    double screenRatio = screenWidth/screenHeight;
    double ratio = height/width;

    return GridView.count(
      childAspectRatio: ratio*screenRatio,
      primary: true,
      crossAxisCount: width,
      children: List.generate(width*height, (index) {
        Color color;
        if (rng.nextInt(101) <= chance)
          color = dangerColor;
        else
          color = defaultColor;
        return Container(
          child: Card(
            color: color,
          ),
        );
      }),
    );
  }

  @override
  void initState() {
    super.initState();
    _widthController = new TextEditingController(text: '$width');
    _heightController = new TextEditingController(text: '$height');
    _percentController = new TextEditingController(text: "$chance");
  }

  Widget build(BuildContext context) {
    double screenWidth = MediaQuery. of(context). size. width;
    double screenHeight = MediaQuery. of(context). size. height;

    return new Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text("Random floor"),
        actions: <Widget>[
      IconButton(
      icon: Icon(
        Icons.settings,
        color: Colors.white,
      ),
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) => _buildPopupDialog(context),
        );
      },
    ),
    ],),
      body: Column(
          children: <Widget>[
        Container(
          height: screenHeight*0.8,
          width: screenWidth*0.8,
            child: grid
        ),
            Container(
              child: RaisedButton(
                child: Text("Button"),
                onPressed: (){
                  setState(() {
                    grid = generateGrid();
                  });
                  },
              ),
            ),
          ],
        ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Settings'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          createSettingField(_widthController, "Width"),
          createSettingField(_heightController, "Height"),
          createSettingField(_percentController, "Chance"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Ok'),
        ),
      ],
    );
  }

  Padding createSettingField(TextEditingController controller, String label){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        onChanged: (text) {
          setState(() {
            if (label == "Width") {
              width = int.parse(text);
            } else if (label == "Height"){
              height = int.parse(text);
            } else if (label == "Chance"){
              chance = int.parse(text);
            }
          });
        },
        controller: controller,
        decoration: InputDecoration(
          labelText: label,
          border: OutlineInputBorder(
          ),
        ),
        keyboardType: TextInputType.number
      ),
    );
  }
}