import 'package:flutter/material.dart';
import 'dart:async';

class CountDown extends StatefulWidget {
  @override
  _CountDown createState() => new _CountDown();
}

class _CountDown extends State<CountDown> {
  Timer? _timer;
  int _start = 0;
  int _original_time = 5;
  Color color = Colors.white;

  void resetTime() {
    _start = _original_time;
    setState(() {
      _start = _start;
    });

    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
    if (_timer == null){
      _timer = new Timer.periodic(
        const Duration(seconds: 1),
            (Timer timer) => setState(
              () {
            if (_start < 1) {
              timer.cancel();
            } else {
              _start = _start - 1;
              if (_start == 0) {
                color = Colors.redAccent;
              }
            }
          },
        ),
      );
    }
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: color,
      appBar: AppBar(
          title: Text("Timer test"),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings,
              color: Colors.white,
            ),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => _buildPopupDialog(context),
              );
            },
          ),
        ],
      ),
      body: Center(child: Column(children: <Widget>[

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Text("$_start"),
          ),
        ),
        Container(
          child: RaisedButton(
            onPressed: () {
              resetTime();
            },
            child: Text("Pull lever"),
          ),
        ),
      ],
      ),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog();
  }
}